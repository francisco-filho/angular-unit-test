describe('MyController', function(){
    beforeEach(module('my-module'))

    describe('getFullName()', function(){
        it('should handle names correctly', inject(function($controller){

            var myController = $controller('MyController')

            myController.firstName = 'julia'
            myController.lastName = 'vitoria'

            myController.getFullName().should.equal('julia vitoria')
        }))
    })
})
