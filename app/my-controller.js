(function(){
    angular.module('my-module',[])
    .controller('MyController', MyController);

    function MyController(){
        var vm = this;
        vm.firstName = ''
        vm.lastName = ''

        vm.getFullName = function(){
            return vm.firstName + ' ' + vm.lastName
        }
    }
})()
