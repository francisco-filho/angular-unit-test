continuous delivery
dsl
databases: realtime, nosql, relational
microservices
distribuited systems
Porte ou criação de algoritmos explorando alto paralelismo em GPUs (Cuda / OpenCL). Ambientes de computação paralela como GPUs e Cloud computing vieram para ficar. Apesar da área de pesquisa já estar meio batida, estas arquiteturas estão cada vez mais presentes em aplicações industriais. Por exemplo, como paralelizar automaticamente um código procedural em uma linguagem qualquer?
Ferramentas para gerência de acões em bolsa de valores e algorítmos de análise e predicão econômicos. Bolhas, investimentos, etc. Como utilizar redes sociais para melhorar as predicões destes algoritmos? Há várias modelos economicos que não foram implementados. Busque artigos de economia e tente modelar / implementar algum modelo.    
Sketch-based interfaces. Como transformar um Sketch em entrada para um sistema?  
Sistemas didáticos para dispositivos infantis (OLPC, Classmate, Cowboy). Como transformar um Laptop em uma ferramenta educacional? Pense em algo como o CodeAcademy, mas mais lúdico ou para crianças. Quem sabe um jogo para ensinar programação? Um Test Driven Development Game?  
Desenvolvimento de linguagens específicas para um domínio (DSL). Otimizar compiladores para ambientes específicos (ex. Cloud Computing) utilizando novas DSLs ou porte de um compilador de linguagem funcional para a VM Java ou DotNet. Criação de DSLs para ampliar características de linguagens, como unir Java e SQL em uma única linguagem ou criar uma DSL para o pipeline gráfico. Extensão de linguagens para especificação de grandezas quando trabalhando com números (m, km, lt,...), para especificação de casas decimais nas variáveis de ponto futuante (Antigo Cobol) ou para incluir instruções de pesquisa em banco de dados nativo (SQL / OQL).  
Tecnologias e testes reais de e-learning
Time Machine for linux: Controle de versão em todos os arquivos do sistema operacional e a possibilidade de retornar a configurações anteriores.

3 - SOFTWARE NA MONITORAÇÃO DA COLABORAÇÃO EM AMBIENTES DE ENSINO 

APLICAÇÕES DO SISTEMA ENTREPISE RESOURCE PLANNING
COMÉRCIO ELETRÔNICO
Abundant-data applications, algorithms, and architectures